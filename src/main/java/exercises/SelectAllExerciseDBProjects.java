package exercises;

import example.ConnectivityOperation;

import java.math.BigInteger;
import java.sql.*;

public class SelectAllExerciseDBProjects extends ConnectivityOperation {

    static public class AllStatements {
        static final public String SELECT_ALL = "SELECT projectId, description AS project FROM projects";
    }

    private static Connection conn;

    // Constructor
    public SelectAllExerciseDBProjects() {

        super();

        String drivers = props.getProperty("jdbc.drivers");
        if (drivers != null) System.setProperty("jdbc.drivers", drivers);
        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");

        try {
            conn = DriverManager.getConnection(url, username, password);
            // SELECT * FROM ...
        } catch (SQLException e) {
            System.out.println("Couldn't connect to DB: " + e.getMessage());
            // e.printStackTrace();
        }
    }

    public static void queryOperation() throws SQLException {
        try {
            new SelectAllExerciseDBProjects().selectAll();

        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
            // e.printStackTrace();
            // log here : lo4j

        } finally {

            if ( null != conn ) {
                conn.close();
            }

        }
    }

    protected void selectAll() throws SQLException {

        ResultSet rs = null;
        Statement statement = null;
        try {

            if (null != conn) {

                statement = conn.createStatement();
                rs = statement.executeQuery(AllStatements.SELECT_ALL);

                // Iterate through results
                while (rs.next()) {

                    BigInteger asBigInteger = BigInteger.valueOf( rs.getLong("projectId") );
                    final Object projectObject = rs.getObject("project");

                    System.out.println( "On row #" + rs.getRow() + " we fetch " +
                            asBigInteger + ": " + projectObject.toString() );
                }

            } else {
                throw new SQLException("Connection could not be initialized.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;

        } finally {

            if ( null != statement ) {
                statement.close();
            }
            if ( null != rs ) {
                rs.close();
            }
        }
    }
}
