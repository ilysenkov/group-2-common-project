package exercises;

import java.sql.SQLException;
import java.util.List;

public class MainExercises {

    public static void main(String[] args) {

        System.out.println("------------SELECT * operation-------------");
        try {
            SelectAllExerciseDBProjects.queryOperation();
        } catch (SQLException e) {
            System.out.println("Query was failed: " + e.getMessage());
        }

        DepartmentRepository departmentRepository = new DepartmentRepository();

        System.out.println("------------LIST ALL DEPS-------------");
        if (!departmentRepository.open()) {
            System.out.println("Can't open datasource");
            return;
        }
        List<Department> departments = departmentRepository.findAll();
        if (departments == null) {
            System.out.println("No departments!");
        }

        System.out.println("------------FIND DEP BY ID-------------");
        if (!departmentRepository.open()) {
            System.out.println("Can't open datasource");
            return;
        }
        departmentRepository.findById(2);

        System.out.println("------------DELETE DEP BY ID-------------");
        if (!departmentRepository.open()) {
            System.out.println("Can't open datasource");
            return;
        }
        departmentRepository.deleteById(3);

    }
}

