package exercises;

import example.ConnectivityOperation;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository extends ConnectivityOperation {

/*  Create class Department mapping the departments table: Integer departmentId, String name
    Create class DepartmentRepository:
        Implement findAll() - returns a list of Department objects representing all table rows
        Implement findById(Integer departmentId) - takes an Integer parameter and returns the Department object corresponding to the provided departmentId
        Implement deleteById(Integer departmentId) - takes an Integer parameter and deletes the table row corresponding to the provided employeeId
        Implement save (Department department) - takes a new Department object parameter with no departmentId and saves the provided department in the database
        Implement update(Department department) - takes an existing Department object parameter (which already has a departmentId that exists in the database) and for the provided departmentId, it updates the name property in the database
        Implement findByName(String name) - takes a String parameter and returns a list of Department objects representing all table rows that have the provided name.
    Optional: Implement ProjectRepository and EmployeeRepository methods findAll, findById, deleteById, save, update */

    public static final String COLUMN_DEPARTMENTS_NAME = "name";
    public static final int INDEX_DEPARTMENT_ID = 1;
    public static final int INDEX_DEPARTMENT_NAME = 2;

    private static Connection conn;

    public boolean open() {
        try {
            conn = DriverManager.getConnection(
                    props.getProperty("jdbc.url"),
                    props.getProperty("jdbc.username"),
                    props.getProperty("jdbc.password"));
            return true;
        } catch (SQLException e) {
            System.out.println("Couldn't connect to DB: " + e.getMessage());
            return false;
        }
    }

    public  void close() {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println("Couldn't close connection: " + e.getMessage());
        }
    }

    public List<Department> findAll() {
        List<Department> departments = new ArrayList<>();
        String selectAllDepartments = "SELECT * FROM departments";

        try (Statement statement = conn.createStatement();
             ResultSet results = statement.executeQuery(selectAllDepartments)) {
            while (results.next()) {
                Integer departmentId = results.getInt(INDEX_DEPARTMENT_ID);
                String depName = results.getString(INDEX_DEPARTMENT_NAME);
                Department department = new Department(departmentId, depName);
                departments.add(department);
                System.out.println("ID: " + department.getDepartmentId() + " | Name: " + department.getName());
            }
            return departments;

        } catch (SQLException e) {
            System.out.println("Query failed:" + e.getMessage());
            return null;
        } finally {
            close();
        }
    }

    public void findById(Integer departmentId) {
        String selectDepartmentsById = "SELECT name FROM departments WHERE departmentId = ?";
        try {
            PreparedStatement prepStatement = conn.prepareStatement(selectDepartmentsById);
            prepStatement.setInt(1, departmentId );
            ResultSet results = prepStatement.executeQuery();
            boolean next = results.next();
            if (next) {
                String name = results.getString(COLUMN_DEPARTMENTS_NAME);
                System.out.println("ID: " + departmentId + " | Name: " + name);
            } else  {
                System.out.println("There is no department with ID " + departmentId);
            }
        } catch (SQLException e) {
            System.out.println("Query failed:" + e.getMessage());
        } finally {
            close();
        }
    }

    public void deleteById(Integer departmentId) {
        String deleteDepartmentById = "DELETE FROM departments WHERE departmentId = ?";
        try {
            PreparedStatement prepStatement = conn.prepareStatement(deleteDepartmentById);
            prepStatement.setInt(1, departmentId );
            prepStatement.executeUpdate();
            boolean next = true;
            if (next) {
                System.out.println("Department with ID " + departmentId + " deleted");
            } else  {
                System.out.println("Couldn't delete department " + departmentId);
            }
        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
        } finally {
            close();
        }

    }
}