package example;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConnectivityOperation {

    final static public String DB_PROPERTIES = "/database.properties";
    protected Properties props = new Properties();

    public ConnectivityOperation() {

            try (final InputStream stream = this.getClass().getResourceAsStream( DB_PROPERTIES ) ) {
                props.load(stream);

                // The line of code is disabled to avoid displaying credentials in the console
                // System.out.println( props );

        } catch ( IOException e) {
            e.printStackTrace();
        }

    }

}
