package example;

import java.sql.*;

public class QueryOperation extends ConnectivityOperation {

    private static Connection conn;

    public QueryOperation() {

        super();

        String drivers = props.getProperty("jdbc.drivers");
        if (drivers != null) System.setProperty("jdbc.drivers", drivers);

        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");

        try {
            QueryOperation.conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void queryOperation(){

        try {

            QueryOperation queryOperation = new QueryOperation();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id, name, email, country FROM user");
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                String country = rs.getString("country");
                System.out.println(id + ", " + name + ", " + email + ", " + country);
            }
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }
}
